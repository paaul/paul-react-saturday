import { combineReducers } from 'redux';
import posts from './posts';
import likes from './likes';

export default combineReducers({
    posts: posts,
    likes: likes,
});

