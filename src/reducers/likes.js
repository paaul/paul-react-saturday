const initialState = ['Vlad', 'Vika']

function likes (state = initialState, action) {
    if (action.type === 'ADD_LIKER') {
        return [...state, action.payload]
    }

    return state;
}

export default likes;