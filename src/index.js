import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import {BrowserRouter as Router} from 'react-router-dom'
import styles from './index.css'

import {createStore} from 'redux'
import { Provider } from 'react-redux';

const store = createStore(reducers, applyMiddleware(trunk))

ReactDOM.render(<Provider ><Router><App /></Router></Provider>, document.getElementById('root'))
