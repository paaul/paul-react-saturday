import React from 'react';
import LikeItem from './LikeItem';
import { connect } from 'react-redux';

class Likes extends React.Component {

    constructor(params) {
        super(params);
        this.state = {
            inputValue: '',
        };
    }

    handleChange = event => {
        this.setState({inputValue: event.target.value})
    }

    handleClick = () => {
        this.props.onHandleClick(this.state.inputValue)
        this.setState ({
            likes:[...this.state.likes, this.state.inputValue]
        })

    }

    render() {
        const list = this.props.likes.map(name => <LikeItem key={name} name={name}/> );
        console.log(list);
        return (
          <div>
              <input type="text" value={this.state.inputValue} onChange={this.handleChange}/>
            <button onClick={this.props.onHandleClick}>Add</button>
            <ul>{list}</ul>
    </div>
    )
    }
}

const mapStateToProps = (state, ownProps = {}) => {
    return {
        likes: state.likes
    }
}

const asyncAction = name => dispatch => {

}

const mapDispatchToProps = (dispatch) => {
    return {
        onHandleClick: name => dispatch({type: 'ADD_LIKER', payload: name}),
        onHandleClickAsync: () => dispatch => {
            setTimeout(dispatch({type: 'ADD_LIKER', payload: name}), 1000)
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Likes)