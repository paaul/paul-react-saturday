import React from 'react';
import {getPost} from "../helpers";
import PropTypes from 'prop-types';


const FeedItem = props => {
    const post = props.post;

    return (
        <div className="post">
            <header>{post.author}</header>
            <img className="post__photo" src={`/im${post.id}.webp`} alt=""/>
            <footer>Likes: {post.likes} <button onClick={()=> props.handleItemClick(post.id)}>Like it!</button></footer>
        </div>
    )

}


FeedItem.propTypes = {
    post: PropTypes.object.isRequired,
    handleItemClick: PropTypes.func.isRequired
};

export default FeedItem;