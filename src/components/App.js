import React from 'react';
import { Route, Link, Switch } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Likes from './Likes';
import Profile from  './Profile';
import Feed from  './Feed';



class App extends React.Component {
    render() {

        return (
            <div>

                <header>
                    <Link to="/">Home</Link>
                    <Link to="/about">About</Link>
                    <Link to="/likes">Likes</Link>
                    <Link to="/profile">Profile</Link>
                    <Link to="/feed">Feed</Link>
                </header>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/likes" component={Likes}/>
                    <Route exact path="/profile" component={Profile}/>
                    <Route path="/feed" component={Feed}/>
                </Switch>
            </div>
        )
    }

}

export default App;