import React from 'react';
import {Link, Route, Switch} from 'react-router-dom'
import FeedItem from './FeedItem'
import FeedList from './FeedList'

           const likes =  [
                {id: '1', author: 'origin', likes: '0'},
                {id: '2', author: 'dan_it', likes: '10'},
                {id: '3', author: 'placebo', likes: '2'}
            ]

      const local = window.localStorage.getItem('likes') ? JSON.parse(window.localStorage.getItem('likes')) : likes;

          class Feed extends React.Component {
              constructor(anyparams){
                  super(anyparams)

                  this.state ={
                      likes: local
                  }
              }

    // getPostById = function(id) {
    //     var likeObj = this.state.likes.find(function(like){
    //         return like.id === id;
    //     })
    //      return likeObj;
    // }

    getPostById = id => this.state.likes.find( like => like.id === id)

    handLike = id => {//this pattern called a chain
        const newLikes = [...this.state.likes]; // ... is a spread
        newLikes
            .find(like => like.id === id)
            .likes++;
        window.localStorage.setItem('likes', JSON.stringify(newLikes));
        this.setState({likes: newLikes})

    }

    render (){
        const {match, history, location} = this.props;
        // const match = this.props.match;
        // const history = this.props.history;
        // const location = this.props.location;
        console.log(match, history, location)

        return (<div>
            <nav>
                <Link to="/feed/1">First</Link>
                <Link to="/feed/2">Second</Link>
                <Link to="/feed/3">Third</Link>
            </nav>
            <Switch>
                {/*<Route exact path={'/feed'} component={FeedList}/>*/}
                {/*<Route path={`/feed/:post`} component={FeedItem}/>*/}
                <Route exact path={match.path} render={props => <FeedList handleClick={this.handLike} posts={this.state.likes}/>}/>
                <Route path={`${match.path}/:id/`} render={(props)=> <FeedItem handleItemClick={this.handLike} post={this.getPostById(props.match.params.id)}/>}/>
            </Switch>
        </div>)
    }
}

export default Feed